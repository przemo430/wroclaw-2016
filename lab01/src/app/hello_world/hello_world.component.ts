import {Component} from 'angular2/core';
import { SecondComponent } from '../second_component/second_component'

@Component({
  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
	templateUrl: 'app/hello_world/hello_world.component.html',
  directives: [SecondComponent]
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  yourName: string = '';
  isMariusz: boolean = true;
}
