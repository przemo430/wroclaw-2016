# Course details

Course on advanced programming techniques.
Topics covered: 

   * Web Technologies: Angular 2 + NodeJS tools
   * Blockchain: technical introduction


# Lectures

   * [Lecture 1](https://docs.google.com/presentation/d/1Wz_szSyHGyPrDQfMca9_ntRRHkC21jLbiOddQ9Akg28/edit?usp=sharing)
   * [Lecture 2](https://docs.google.com/presentation/d/1zxme1rtnNt5P0rVYyhXKJMVKt6kP7fTy2oBeRRPD89E/edit?usp=sharing)
   * [Lecture 3](https://ipfs.io/#alpha-demo) IPFS alpha demo
   * [Lecture 4](https://www.youtube.com/watch?v=Lx9zgZCMqXE) Bitcoin Basics
   * [Lecture 5](https://www.youtube.com/watch?v=glyQy_e5LmM) Bitcoin Anonymity
   * [Lecture 6](https://www.youtube.com/watch?v=U_LK0t_qaPo) Ethereum Basics
   * [Lecture 7](https://www.youtube.com/watch?v=gjwr-7PgpN8) Ethereum technical intro
   * [Lecture 8](http://victorsavkin.com/post/118372404541/the-core-concepts-of-angular-2) Core concepts of AngularJS 2.0
   


# Labs

## Lab 00

   1. Install NodeJS on your laptop/computer. Version 5.10.1
   2. Install 'gulp' globally on your system, by running: *npm install -g gulp*        
   3. Go and check the Hello World tutorial on [https://angular.io](https://angular.io/)  
   4. Try to replicate the code, and get it to run on your laptop.

## Lab 01

   1. Go to lab01 in this repo
   2. Run

```
#!bash

npm install
gulp
gulp serve
```

Notes: it is OK to accept the two WARNINGS about version mismatch - the newer versions have certain things fixed, better to use the newer versions. 


## Lab 02

   * Download and install [Ethereum-Wallet](https://github.com/ethereum/mist/releases). The wallet has the Ethereum Go node and CPU miner built-in. Use it to setup your account and mine some Ether on the **Test-Net** network.
   * Setup your account, and sync up with the Test-Net. You can connect to your own Ethereum node if you followed the optional steps below.
   * Optional steps: [Download and install](https://github.com/ethereum/go-ethereum/wiki/Building-Ethereum) Ethereum Go client. Launch on a test network. [Download and install](https://github.com/ethereum/go-ethereum/wiki/mining) C++ Ethereum miner. Launch on a test network. Mine some ether.
   * Send some testnet Ether from one account to another within your own client.
   * Setup a wallet (once you have some Ether in the account). Transfer funds between wallet addresses. 
   * Send some testnet Ether to your friends.
   


## Lab 03

   * [Tutorial for crypto-tokens](https://www.ethereum.org/token)


# Homeworks

## Task 1 - passed

In this folder

        QmZMTpjjDBc3RzQsNR4tnh6Wu7iTG5BPB9N1PAnL27VLcp

create a file: *yourname.surname-email.txt*
and put your email inside. Add your newly created file to IPFS. Remember the new hash somewhere.


## Task 2 - passed

   1. Clone the fork of this repo.
   2. Make a pull-request to upstream, with your name added to students.txt file.


## Task 3 - deadline: May 20

   1. Modify Lab01 so that it uses Twitter Bootstrap, or Google's Material Design UI elements.
   2. Make it look *nicer*.
   3. Make a pull request with your changes.


## Task 4 - deadline: May 22

   1. Add to Lab01 another component on top of the page (make this a class in a separate .ts file), that provides a simple notification message to the user. 
   2. Normally, the notification is invisible, but when the field 'yourName' of the HelloWorld component has a value 'Mariusz', make the notification to show up with a message to the user 'Mariusz is lecturing in C025'
   3. Make a pull request with your working solution.


## Task 5 - deadline: May 22

   1. Modify Task 4 in such a way, that user is presented with two additional text input fields: one for the pattern to be detected from 'yourName', and the other for the notification when the pattern is found in 'yourName'. 
   2. Make the app dynamically react with the given notification based on the detection of a given pattern (in analogy to task 4).
   3. Make a pull request with your working solution.


## Task 6 - deadline: June 1

   1. Add to the repo new folder. The folder name will be a unique group name of your group, followed by the text '_task'. Groups of 3 and 4 students are allowed. 
   2. In Angular 2.0, implement client-side application that works like a registry, or hash map. The registry will allow users to store IPFS file hashes mapped to human-readable names, and Bitcoin and Ethereum addresses, mapped to human-readable names. 
   3. Make the app deployed on IPFS, and use browser-side storage only. 
   4. Attach a short Readme.md file with the student names of your group, and provide a short report describing how the app could be modified to use IPFS for storage instead of client-side browser datastore, so that multiple users could share the same registry of hash<->human_readable names. 
   5. Submit a pull request with your solution. 

## Task 7 - bonus

   1. If you do not have any TestNet Ether or Bitcoin, create an issue assigned to Mariusz, and provide your Bitcoin or Ethereum TestNet address. Mariusz will send you some testnet coins.
   2. Send a small, random amount of coins to Mariusz. For each transfer, create an Issue in the tracker, specifying what amount have you transferred (so that Mariusz can match the transfer to the person).
   3. Bitcoin Testnet address to send coins to: n2sY6i31eUnqYPCgqdfefjaC13Zs4Lf3Zt
   4. Ethereum wallet address: 0xF88b36F7569519eE8f8e272FBA81021f3498A38c


# Project - deadline: June 5

   1. Add to the repo new folder. The folder name will be a unique group name of your group same as in Task 6, followed by the text '_project'. Groups of 3 and 4 students are allowed. 
   2. You have to plan and design a new workflow system for software development teams. Imagine a team of developers working on a single software project, for example a mobile game. The number of developers is unknown at the start of the project. New developers might join in, and existing developers might bail out of the project. Once the game is finished, it will be published on Google Play, and the sales will be automatically monitored via the Google API. The development is done through a GIT repository, eg. on GitHub. Based on the blockchain technology, design a system that will monitor everyone's contributions to the project and reward everyone according to the rules set forth before they have committed their work. Think about proposal submission, voting mechanism, and the rules that will govern this distributed team. Make sure the system is fair, verifiable, and cannot be (easily) abused. Pay attention to the distributed governance model, voting and consensus, and what indicators can be integrated into the system (eg. GitHub commits/lines added/lines removed, issues closed, issues created, Google API for app downloads, sales, etc).
   3. Place your report, with all auxiliary files (design diagrams, graphs, etc) in the folder from point 1.
   4. Submit a pull request with your report within that subfolder.