import {Component} from 'angular2/core';


@Component({
  // Declare the tag name in index.html to where the component attaches
    selector: 'second-component',
	templateUrl: 'app/second_component/second_component.html'
})

export class SecondComponent{
    secondFieldContent: string = "Second field conent";
}